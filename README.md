# Konfiture

[![build status](https://gitlab.com/konfiture/konfiture/badges/master/build.svg)](https://gitlab.com/konfiture/konfiture/commits/master)
[![coverage](https://gitlab.com/konfiture/konfiture/badges/master/coverage.svg?job=coverage)](https://konfiture.gitlab.io/konfiture/coverage)
[![PyPI version](https://badge.fury.io/py/konfiture.svg)](https://badge.fury.io/py/konfiture)

Konfiture is a speller and grammar checker for markdown files.

Need more info, look at the homepage documentation. [konfiture.gitlab.io](http://konfiture.gitlab.io/)

## Install

```
pip3 install konfiture
```

## Quickstart

```
konfiture file0.md [file1.md...]
```
